package com.app.basicweatherforecast.data.model.weather

data class Wind(
    val deg: Int,
    val speed: Double
)