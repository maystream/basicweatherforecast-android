package com.app.basicweatherforecast.data.model

data class Hourly(
    val clouds: Int,
    val dew_point: Double,
    val dt: Long,
    val feels_like: String,
    val humidity: String,
    val pop: Double,
    val pressure: Int,
    val rain: Rain,
    val temp: String,
    val visibility: Int,
    val weather: List<WeatherX>,
    val wind_deg: Int,
    val wind_speed: Double
)