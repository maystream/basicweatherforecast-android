package com.app.basicweatherforecast.data.model.weather

data class Clouds(
    val all: Int
)