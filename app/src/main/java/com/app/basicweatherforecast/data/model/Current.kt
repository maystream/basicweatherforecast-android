package com.app.basicweatherforecast.data.model

data class Current(
    val clouds: Int,
    val dew_point: Double,
    val dt: Long,
    val feels_like: String,
    val humidity: String,
    val pressure: Int,
    val sunrise: Int,
    val sunset: Int,
    val temp: String,
    val uvi: Double,
    val visibility: Int,
    val weather: List<Weather>,
    val wind_deg: Int,
    val wind_speed: Double
)