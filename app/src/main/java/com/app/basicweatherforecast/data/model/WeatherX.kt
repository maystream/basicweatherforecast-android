package com.app.basicweatherforecast.data.model

data class WeatherX(
    val description: String,
    val icon: String,
    val id: Int,
    val main: String
)