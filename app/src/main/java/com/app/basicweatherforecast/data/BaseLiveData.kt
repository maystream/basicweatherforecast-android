package com.app.basicweatherforecast.data

data class BaseLiveData<T>(
    var error: String? = null,
    var data: T? = null,
    var listener: () -> Unit = {}
) {
    fun dataIsNotNull() = data != null
}