package com.app.basicweatherforecast.data.model

data class WeatherResponse(
    val current: Current,
    val hourly: List<Hourly>,
    val lat: Double,
    val lon: Double,
    val timezone: String,
    val timezone_offset: Int
)