package com.app.basicweatherforecast.data.model

data class Rain(
    val `1h`: Double
)