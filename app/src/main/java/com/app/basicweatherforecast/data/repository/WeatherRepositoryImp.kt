package com.app.basicweatherforecast.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.basicweatherforecast.data.BaseLiveData
import com.app.basicweatherforecast.data.Resource
import com.app.basicweatherforecast.data.model.WeatherResponse
import com.app.basicweatherforecast.data.model.weather.WeatherNowResponse
import com.app.basicweatherforecast.data.remote.api.WeatherApi
import com.app.basicweatherforecast.utils.Constant.API_KEY_WEATHER
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class WeatherRepositoryImp(private val api: WeatherApi): WeatherRepository {
    private val compositeDisposable = CompositeDisposable()

    override fun getCurrentWeather(
        lat: String,
        lon: String,
        units: String
    ): MutableLiveData<Resource<WeatherNowResponse>> {
        val data = MutableLiveData<Resource<WeatherNowResponse>>()
        data.postValue(Resource.loading(null))
        val disposable = api.getCurrentWeather(lat = lat, lon = lon, units = units, appId = API_KEY_WEATHER)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                Log.d("getWeather", "Success")
                data.postValue(Resource.success("", it))
            }, {
                Log.d("getWeather", "error= ${it.message}")
                data.postValue(Resource.error(0, "${it.message}", null))
            })
        compositeDisposable.add(disposable)
        return data
    }

    override fun getForecast(
        lat: String,
        lon: String,
        units: String
    ): LiveData<Resource<WeatherResponse>> {
        val data = MutableLiveData<Resource<WeatherResponse>>()
        data.postValue(Resource.loading(null))
        val disposable = api.getForecast(lat = lat, lon = lon, units = units, appId = API_KEY_WEATHER)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                Log.d("getWeather", "Success")
                data.postValue(Resource.success("", it))
            }, {
                Log.d("getWeather", "error= ${it.message}")
                data.postValue(Resource.error(0, "${it.message}", null))
            })
        compositeDisposable.add(disposable)
        return data
    }

    override fun clear() {
        compositeDisposable.dispose()
    }

}