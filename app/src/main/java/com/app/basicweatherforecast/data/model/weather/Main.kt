package com.app.basicweatherforecast.data.model.weather

data class Main(
    val feels_like: String,
    val humidity: String,
    val pressure: Int,
    val temp: String,
    val temp_max: String,
    val temp_min: String
)