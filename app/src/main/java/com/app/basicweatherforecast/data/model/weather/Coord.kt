package com.app.basicweatherforecast.data.model.weather

data class Coord(
    val lat: Double,
    val lon: Double
)