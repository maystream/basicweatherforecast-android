package com.app.basicweatherforecast.data

data class Resource<out T>(var status: Status, val data: T?, val code: Int?, val message: String?) {

    companion object {

        fun <T> success(msg: String, data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, 0,msg)
        }

        fun <T> error(code: Int?, msg: String, data: T?): Resource<T> {
            return Resource(Status.ERROR, data, code, msg)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, 0,null)
        }
    }
}

enum class Status {
    SUCCESS,
    ERROR,
    LOADING,
}