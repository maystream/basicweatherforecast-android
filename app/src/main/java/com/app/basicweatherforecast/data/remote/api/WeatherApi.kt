package com.app.basicweatherforecast.data.remote.api

import com.app.basicweatherforecast.data.model.WeatherResponse
import com.app.basicweatherforecast.data.model.weather.WeatherNowResponse
import com.app.basicweatherforecast.utils.Constant.CURRENT
import com.app.basicweatherforecast.utils.Constant.FORECAST
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET(CURRENT)
    fun getCurrentWeather(
        @Query("lat") lat: String,
        @Query("lon") lon: String,
        @Query("units") units: String,
        @Query("appid") appId: String
    ): Observable<WeatherNowResponse>

    @GET(FORECAST)
    fun getForecast(
        @Query("lat") lat: String,
        @Query("lon") lon: String,
        @Query("exclude") exclude: String = "minutely,daily",
        @Query("units") units: String,
        @Query("appid") appId: String
    ): Observable<WeatherResponse>

}