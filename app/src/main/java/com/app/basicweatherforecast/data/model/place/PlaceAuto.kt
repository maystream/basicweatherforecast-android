package com.app.basicweatherforecast.data.model.place

data class PlaceAuto(
    var placeId: String,
    var primary: String,
    var secondary: String,
    var lat: Double,
    var lon: Double
)