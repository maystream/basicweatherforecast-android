package com.app.basicweatherforecast.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.basicweatherforecast.data.BaseLiveData
import com.app.basicweatherforecast.data.Resource
import com.app.basicweatherforecast.data.model.WeatherResponse
import com.app.basicweatherforecast.data.model.weather.WeatherNowResponse

interface WeatherRepository {

    fun getCurrentWeather(lat: String, lon: String, units: String): LiveData<Resource<WeatherNowResponse>>

    fun getForecast(lat: String, lon: String, units: String): LiveData<Resource<WeatherResponse>>

    fun clear()

}