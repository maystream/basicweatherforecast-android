package com.app.basicweatherforecast.data.remote.interceptor

import android.content.Context
import android.net.ConnectivityManager

class LiveNetworkMonitor(private val context: Context) {

    fun isConnected(): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }
}