package com.app.basicweatherforecast.ui.today

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.app.basicweatherforecast.data.BaseLiveData
import com.app.basicweatherforecast.data.Resource
import com.app.basicweatherforecast.data.model.WeatherResponse
import com.app.basicweatherforecast.data.model.weather.WeatherNowResponse
import com.app.basicweatherforecast.data.repository.WeatherRepository
import com.app.basicweatherforecast.utils.AbsentLiveData
import com.app.basicweatherforecast.utils.Units
import com.app.basicweatherforecast.utils.WeatherUtil
import com.google.android.gms.maps.model.LatLng

class TodayWeatherViewModel(private val repo: WeatherRepository): ViewModel() {

    var triggerLatLng = MutableLiveData<LatLng>()
    val loadWeather: LiveData<Resource<WeatherResponse>> = Transformations
        .switchMap(triggerLatLng) { trigger ->
            if (trigger == null) {
                AbsentLiveData.create()
            }
            else {
                repo.getForecast(WeatherUtil.latitude!!,
                    WeatherUtil.longitude!!, WeatherUtil.units!!)
            }
        }

    override fun onCleared() {
        super.onCleared()
        repo.clear()
    }
}