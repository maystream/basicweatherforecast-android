package com.app.basicweatherforecast.ui.weather.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.app.basicweatherforecast.R
import com.app.basicweatherforecast.data.model.place.PlaceAuto
import kotlinx.android.synthetic.main.item_search.view.*

class SearchAdapter(private val clickListener: (PlaceAuto) -> Unit)
    : ListAdapter<PlaceAuto, SearchAdapter.ViewHolder>(AddressDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.item_search, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }

    class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(address: PlaceAuto, clickListener: (PlaceAuto) -> Unit) {
            view.apply {
                textLocationName.text = address.primary
                setOnClickListener { clickListener.invoke(address) }
            }
        }
    }

    class AddressDiffCallback : DiffUtil.ItemCallback<PlaceAuto>() {

        override fun areItemsTheSame(oldItem: PlaceAuto, newItem: PlaceAuto): Boolean {
            return oldItem.placeId == newItem.placeId
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: PlaceAuto, newItem: PlaceAuto): Boolean {
            return oldItem == newItem
        }
    }
}