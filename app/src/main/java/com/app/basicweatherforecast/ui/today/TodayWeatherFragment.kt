package com.app.basicweatherforecast.ui.today

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.app.basicweatherforecast.R
import com.app.basicweatherforecast.data.Status
import com.app.basicweatherforecast.data.model.WeatherResponse
import com.app.basicweatherforecast.databinding.FragmentToDayWeatherBinding
import com.app.basicweatherforecast.ui.today.adapter.TodayWeatherAdapter
import com.app.basicweatherforecast.utils.WeatherUtil
import com.google.android.gms.maps.model.LatLng
import org.koin.androidx.viewmodel.ext.android.viewModel

class TodayWeatherFragment : Fragment() {

    private lateinit var binding: FragmentToDayWeatherBinding
    private val viewModel: TodayWeatherViewModel by viewModel()
    private val adapterToday = TodayWeatherAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentToDayWeatherBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInstance()
        fetchData()

        val latLng = LatLng(WeatherUtil.latitude!!.toDouble(), WeatherUtil.longitude!!.toDouble())
        viewModel.triggerLatLng.postValue(latLng)
    }

    private fun initInstance() {
        binding.recyclerview.adapter = adapterToday
    }

    private fun fetchData() {
        viewModel.loadWeather.observe(viewLifecycleOwner, Observer {
            when(it?.status) {
                Status.LOADING -> { }
                Status.SUCCESS -> {
                    it.data?.apply {
                        setWeatherData(it.data)
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(context, getString(R.string.load_fail), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun setWeatherData(data: WeatherResponse) {
        adapterToday.apply {
            submitList(data.hourly)
        }
    }

}