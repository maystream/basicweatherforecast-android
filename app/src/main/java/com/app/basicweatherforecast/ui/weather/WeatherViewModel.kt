package com.app.basicweatherforecast.ui.weather

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.app.basicweatherforecast.data.Resource
import com.app.basicweatherforecast.data.model.weather.WeatherNowResponse
import com.app.basicweatherforecast.data.repository.WeatherRepository
import com.app.basicweatherforecast.utils.AbsentLiveData
import com.app.basicweatherforecast.utils.WeatherUtil
import com.google.android.gms.maps.model.LatLng


class WeatherViewModel(private val repo: WeatherRepository): ViewModel() {

    var unit = ""
    var triggerLatLng = MutableLiveData<LatLng>()

    val loadWeather: LiveData<Resource<WeatherNowResponse>> = Transformations
        .switchMap(triggerLatLng) { trigger ->
            if (trigger == null) {
                AbsentLiveData.create()
            }
            else {
                repo.getCurrentWeather(WeatherUtil.latitude!!,
                    WeatherUtil.longitude!!, unit)
            }
        }

    override fun onCleared() {
        super.onCleared()
        repo.clear()
    }
}