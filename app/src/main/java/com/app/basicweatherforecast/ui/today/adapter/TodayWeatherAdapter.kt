package com.app.basicweatherforecast.ui.today.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.app.basicweatherforecast.R
import com.app.basicweatherforecast.data.model.Hourly
import com.app.basicweatherforecast.utils.*
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_today_weather.view.*

class TodayWeatherAdapter: ListAdapter<Hourly, TodayWeatherAdapter.HourlyViewHolder>(HourlyDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HourlyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return HourlyViewHolder(inflater.inflate(R.layout.item_today_weather, parent, false))
    }

    override fun onBindViewHolder(holder: HourlyViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class HourlyViewHolder(private val view: View): RecyclerView.ViewHolder(view) {

        @SuppressLint("SetTextI18n")
        fun bind(item: Hourly) {
            view.apply {
                dateTimeTextView.text = item.dt.toFormatString()
                val units = if (WeatherUtil.units == Units.CELSIUS.value) Units.CELSIUS.shortName else Units.FAHRENHEIT.shortName
                tempTextView.text = "${item.temp.toCelsius()} $units"
                humidityTextView.text = "${context.getString(R.string.weather_humidity)} ${item.humidity.toPercent()}"

                val iconUrl = "${Constant.BASE_URL_IMG}${item.weather[0].icon.toPNG()}"
                Glide.with(context).load(iconUrl).into(imvWeather)
            }
        }
    }

    class HourlyDiffCallback : DiffUtil.ItemCallback<Hourly>() {

        override fun areItemsTheSame(oldItem: Hourly, newItem: Hourly): Boolean {
            return oldItem.dt == newItem.dt
        }

        override fun areContentsTheSame(oldItem: Hourly, newItem: Hourly): Boolean {
            return oldItem == newItem
        }
    }
}