package com.app.basicweatherforecast.ui.setting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import com.app.basicweatherforecast.R
import com.app.basicweatherforecast.ui.base.BaseFragment
import com.app.basicweatherforecast.utils.Units
import com.app.basicweatherforecast.utils.WeatherUtil
import kotlinx.android.synthetic.main.fragment_setting.*

class SettingFragment : BaseFragment() {

    companion object {
        fun newInstance() = SettingFragment().apply {
            arguments = Bundle()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_setting, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initInstance()
        setUnitsData()
    }

    private fun initInstance() {
        backBtn.setOnClickListener {
            activity?.finish()
        }
        unitsView.setOnClickListener { showDialogUnit() }
    }

    private fun setUnitsData () {
        when(WeatherUtil.units) {
            Units.FAHRENHEIT.value -> unitsTextView.text = Units.FAHRENHEIT.nameUnit
            Units.CELSIUS.value -> unitsTextView.text = Units.CELSIUS.nameUnit
        }
    }

    private fun showDialogUnit() {
        val builder = context?.let { AlertDialog.Builder(it) }
        builder?.setTitle(getString(R.string.setting_units))

        val units = arrayOf(Units.FAHRENHEIT.nameUnit, Units.CELSIUS.nameUnit)
        builder?.setItems(units) { dialog, which ->
            when (which) {
                0 -> WeatherUtil.units = Units.FAHRENHEIT.value
                1 -> WeatherUtil.units = Units.CELSIUS.value
            }

            setUnitsData()
        }

        val dialog = builder?.create()
        dialog?.show()
    }
}