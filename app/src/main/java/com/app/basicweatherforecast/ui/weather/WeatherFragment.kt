package com.app.basicweatherforecast.ui.weather

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.basicweatherforecast.R
import com.app.basicweatherforecast.data.Status
import com.app.basicweatherforecast.data.model.place.PlaceAuto
import com.app.basicweatherforecast.data.model.weather.WeatherNowResponse
import com.app.basicweatherforecast.databinding.FragmentWeatherBinding
import com.app.basicweatherforecast.ui.base.BaseFragment
import com.app.basicweatherforecast.ui.weather.adapter.SearchAdapter
import com.app.basicweatherforecast.utils.*
import com.app.basicweatherforecast.utils.Constant.BASE_URL_IMG
import com.bumptech.glide.Glide
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.api.net.*
import kotlinx.android.synthetic.main.fragment_weather.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class WeatherFragment : BaseFragment() {

    private lateinit var binding: FragmentWeatherBinding
    private val viewModel: WeatherViewModel by viewModel()

    private var locationManager: LocationManager? = null
    private lateinit var searchAdapter: SearchAdapter

    private var placesClient: PlacesClient? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentWeatherBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInstance()
        initListener()
    }

    private fun initInstance() {
        fetchData()
        viewModel.unit = WeatherUtil.units!!
        context?.let { Places.initialize(it, getString(R.string.api_key)) }
        placesClient = context?.let { Places.createClient(it) }

        locationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager?

        if (WeatherUtil.latitude.isNullOrEmpty() || WeatherUtil.longitude.isNullOrEmpty()){
            getLocation()
        } else {
            val latLng = LatLng(
                WeatherUtil.latitude!!.toDouble(),
                WeatherUtil.longitude!!.toDouble()
            )
            viewModel.triggerLatLng.postValue(latLng)
        }
        setAdapter()
    }

    private fun initListener() {
        binding.apply {
            searchBtn.setOnClickListener { onSearchClick() }
            settingBtn.setOnClickListener { onSettingClick() }

            recyclerView.apply {
                layoutManager = LinearLayoutManager(activity)
                adapter = searchAdapter
                setHasFixedSize(true)
            }

            searchEdit.setOnEditorActionListener(object : TextView.OnEditorActionListener {
                override fun onEditorAction(
                    v: TextView?,
                    actionId: Int,
                    event: KeyEvent?
                ): Boolean {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        if (searchEdit.text?.toString()?.isNotEmpty()!!) {
                            searchBtn.performClick()
                            context?.let { hideKeyboard(it, searchEdit) }
                            searchEdit.isFocusable = true
                        }
                        return true
                    }
                    return false
                }
            })
        }
    }

    private fun setAdapter() {
        searchAdapter = SearchAdapter(clickListener = { address ->
            clearListLocation(address)
        })
    }

    private fun clearListLocation(address: PlaceAuto) {
        searchAdapter.submitList(null)
        searchEdit.text?.clear()
        recyclerView.visibility = View.GONE
        getPlaceById(address.placeId)
    }

    private fun getAutocompletePredictions(query: String) {
        val token: AutocompleteSessionToken = AutocompleteSessionToken.newInstance()

        val request: FindAutocompletePredictionsRequest =
            FindAutocompletePredictionsRequest.builder()
                .setTypeFilter(TypeFilter.ADDRESS)
                .setSessionToken(token)
                .setQuery(query)
                .build()

        placesClient!!.findAutocompletePredictions(request)
            .addOnSuccessListener { response: FindAutocompletePredictionsResponse ->
                val resultList = ArrayList<PlaceAuto>()
                for (prediction in response.autocompletePredictions) {
                    resultList.add(
                        PlaceAuto(
                            prediction.placeId,
                            prediction.getPrimaryText(null).toString(),
                            prediction.getSecondaryText(null).toString(),
                            0.0,
                            0.0
                        )
                    )
                }
                searchAdapter.submitList(resultList)
                recyclerView.visibility = View.VISIBLE
            }.addOnFailureListener { exception: Exception ->
            if (exception is ApiException) {
                val apiException: ApiException = exception
                Log.e("TAG", "Place not found: " + apiException.statusCode)
            }
        }
    }

    private fun getPlaceById(placeId: String) {
        val placeFields: List<Place.Field> = listOf(Place.Field.ID, Place.Field.LAT_LNG)
        val request: FetchPlaceRequest = FetchPlaceRequest.builder(placeId, placeFields)
            .build()

        placesClient!!.fetchPlace(request).addOnSuccessListener { response: FetchPlaceResponse ->
            val place = response.place
            val latLng = place.latLng

            latLng?.apply {
                Log.i("TAG", "latLng: " + latLng.latitude.toString() + latLng.longitude.toString())
                WeatherUtil.latitude = latLng.latitude.toString()
                WeatherUtil.longitude = latLng.longitude.toString()

                viewModel.triggerLatLng.postValue(latLng)
            }

        }.addOnFailureListener { exception: java.lang.Exception ->
            if (exception is ApiException) {
                val apiException = exception as ApiException
                val statusCode = apiException.statusCode

                Log.e("TAG", "Place not found: " + exception.message)
            }
        }
    }

    private fun fetchData() {
        viewModel.loadWeather.observe(viewLifecycleOwner, Observer {
            when (it?.status) {
                Status.LOADING -> {
                }
                Status.SUCCESS -> {
                    it.data?.apply {
                        setWeatherData(it.data)
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(context, getString(R.string.load_fail), Toast.LENGTH_SHORT)
                        .show()
                }
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun setWeatherData(data: WeatherNowResponse) {
        binding.apply {
            val units = if (WeatherUtil.units == Units.CELSIUS.value) Units.CELSIUS.shortName else Units.FAHRENHEIT.shortName
            tempTextView.text = "${data.main.temp.toCelsius()} $units"
            dateTimeTextView.text = data.dt.toFormatDateTime()
            locationTextView.text = data.name
            humidityTextView.text = "${getString(R.string.weather_humidity)} ${data.main.humidity.toPercent()}"

            val iconUrl = "${BASE_URL_IMG}${data.weather[0].icon.toPNG()}"
            Glide.with(requireContext()).load(iconUrl).into(imvWeather)
        }
    }

    private fun onSearchClick() {
        if (searchEdit.text.toString().isNotEmpty()) {
            getAutocompletePredictions(searchEdit.text.toString())
        }
    }

    private fun onSettingClick() {
        findNavController().navigate(R.id.action_weatherFragment_to_settingActivity)
    }

    private fun getLocation() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(), Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION
            )!= PackageManager.PERMISSION_GRANTED
        ) {
            locationPermission(listener = { report ->
                when {
                    report.areAllPermissionsGranted() -> {
                        getLocation()
                    }
                    else -> {
                        Toast.makeText(
                            context,
                            getString(R.string.unable_to_location),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
            })
        } else {
            val locationGPS: Location? =
                locationManager?.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            if (locationGPS != null) {

                val lat: Double = locationGPS.latitude
                val long: Double = locationGPS.longitude

                WeatherUtil.latitude = lat.toString()
                WeatherUtil.longitude = long.toString()

                if (WeatherUtil.latitude?.isNotEmpty()!! && WeatherUtil.longitude?.isNotEmpty()!!) {
                    val latLng = LatLng(
                        WeatherUtil.latitude!!.toDouble(),
                        WeatherUtil.longitude!!.toDouble()
                    )
                    viewModel.triggerLatLng.value = latLng
                }

            } else {
                Toast.makeText(context, getString(R.string.unable_to_location), Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (viewModel.unit != WeatherUtil.units!!) {
            val latLng = LatLng(
                WeatherUtil.latitude!!.toDouble(),
                WeatherUtil.longitude!!.toDouble()
            )
            viewModel.unit = WeatherUtil.units!!
            viewModel.triggerLatLng.postValue(latLng)
        }
    }
}
