package com.app.basicweatherforecast.ui.setting

import android.os.Bundle
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.app.basicweatherforecast.R


class SettingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        if (savedInstanceState == null)
            supportFragmentManager.beginTransaction()
                .add(R.id.contentContainer, SettingFragment.newInstance())
                .commit()
    }
}