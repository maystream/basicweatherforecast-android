package com.app.basicweatherforecast.utils

import android.content.Context
import android.content.SharedPreferences
import com.app.basicweatherforecast.utils.Constant.LAT_KEY
import com.app.basicweatherforecast.utils.Constant.LON_KEY
import com.app.basicweatherforecast.utils.Constant.UNITS_KEY

object WeatherUtil {

    private lateinit var context: Context

    fun init(context: Context) {
        this.context = context
    }

    private val sp: SharedPreferences by lazy { context.getSharedPreferences("WeatherPreference", Context.MODE_PRIVATE) }

    var latitude: String?
        get() = sp.getString(LAT_KEY, "")
        set(value) {
            sp.edit().putString(LAT_KEY, value).apply()
        }

    var longitude: String?
        get() = sp.getString(LON_KEY, "")
        set(value) {
            sp.edit().putString(LON_KEY, value).apply()
        }

    var units: String?
        get() = sp.getString(UNITS_KEY, Units.CELSIUS.value)
        set(value) {
            sp.edit().putString(UNITS_KEY, value).apply()
        }

}