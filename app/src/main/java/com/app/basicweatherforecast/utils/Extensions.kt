package com.app.basicweatherforecast.utils

import java.sql.Timestamp
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


enum class Units(val value: String, val shortName: String, val nameUnit: String) {
    FAHRENHEIT("imperial", "F", "imperial( ํF )"),
    CELSIUS("metric", "C", "metric( ํC )")
}

fun String.toPercent(): String {
    return "$this %"
}

fun String.toCelsius(): String {
    return "$this ํ"
}

fun String.toPNG(): String {
    return "$this.png"
}

fun Long.toFormatDateTime(): String {
    val formatter = SimpleDateFormat("EEE, MMM d, hh:mm aaa", Locale.US)
    val stamp = Timestamp(this * 1000L)
    val date = Date(stamp.time)
    return formatter.format(date)
}

fun Long.toFormatString(): String {
    val formatter: DateFormat = SimpleDateFormat("h a", Locale.US)
    val stamp = Timestamp(this * 1000L)
    val date = Date(stamp.time)
    return formatter.format(date)
}