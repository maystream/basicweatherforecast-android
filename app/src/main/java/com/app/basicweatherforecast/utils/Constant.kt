package com.app.basicweatherforecast.utils

object Constant {
    const val BASE_URL = "https://api.openweathermap.org/"
    const val BASE_URL_IMG = "https://api.openweathermap.org/img/w/"
    const val API_KEY_WEATHER = "468e1543243d91db74a8f28947fb6c8f"

    const val CURRENT = "data/2.5/weather"
    const val FORECAST = "data/2.5/onecall"

    const val CONNECT_TIMEOUT_IN_SECONDS = 10
    const val READ_TIMEOUT_IN_SECONDS = 60
    const val WRITE_TIMEOUT_IN_SECONDS = 60
    const val CACHE_SIZE_BYTES = 10 * 1024 * 1024L // 10 MB

    const val LAT_KEY = "LAT_KEY"
    const val LON_KEY = "LON_KEY"
    const val UNITS_KEY = "UNITS_KEY"
}