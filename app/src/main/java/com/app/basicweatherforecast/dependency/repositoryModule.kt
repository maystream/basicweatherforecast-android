package com.app.basicweatherforecast.dependency

import com.app.basicweatherforecast.data.repository.WeatherRepository
import com.app.basicweatherforecast.data.repository.WeatherRepositoryImp
import org.koin.dsl.module

val repositoryModule = module {
    factory { WeatherRepositoryImp(get())}
    factory<WeatherRepository> { WeatherRepositoryImp(get()) }
}