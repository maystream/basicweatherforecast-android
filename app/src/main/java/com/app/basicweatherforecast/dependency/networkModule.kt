package com.app.basicweatherforecast.dependency

import android.content.Context
import com.app.basicweatherforecast.data.remote.api.WeatherApi
import com.app.basicweatherforecast.data.remote.interceptor.LiveNetworkMonitor
import com.app.basicweatherforecast.data.remote.interceptor.NetworkInterceptor
import com.app.basicweatherforecast.utils.Constant.BASE_URL
import com.app.basicweatherforecast.utils.Constant.CACHE_SIZE_BYTES
import com.app.basicweatherforecast.utils.Constant.CONNECT_TIMEOUT_IN_SECONDS
import com.app.basicweatherforecast.utils.Constant.READ_TIMEOUT_IN_SECONDS
import com.app.basicweatherforecast.utils.Constant.WRITE_TIMEOUT_IN_SECONDS
import okhttp3.Cache
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {

    single { NetworkInterceptor(LiveNetworkMonitor(androidContext())) }
    single { provideOkHttpClient(androidContext(), get()) }
    single { provideRetrofit(get()) }
    single { provideWeatherApiService(get()) }
}

fun provideOkHttpClient(context: Context, networkInterceptor: NetworkInterceptor): OkHttpClient {

    val clientBuilder = OkHttpClient.Builder().apply {
        cache(Cache(context.cacheDir, CACHE_SIZE_BYTES))
        readTimeout(READ_TIMEOUT_IN_SECONDS.toLong(), TimeUnit.SECONDS)
        writeTimeout(WRITE_TIMEOUT_IN_SECONDS.toLong(), TimeUnit.SECONDS)
        connectTimeout(CONNECT_TIMEOUT_IN_SECONDS.toLong(), TimeUnit.SECONDS)
        addInterceptor(networkInterceptor)
        addInterceptor(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        })
    }

    return clientBuilder.build()
}

fun provideRetrofit(client: OkHttpClient): Retrofit {

    return Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}

fun provideWeatherApiService(retrofit: Retrofit): WeatherApi = retrofit.create(
    WeatherApi::class.java
)