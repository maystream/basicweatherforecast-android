package com.app.basicweatherforecast.dependency

import com.app.basicweatherforecast.ui.today.TodayWeatherViewModel
import com.app.basicweatherforecast.ui.weather.WeatherViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    viewModel { WeatherViewModel(get()) }
    viewModel { TodayWeatherViewModel(get()) }
}